#ifndef CUB_H
# define CUB_H

/*
** External library
*/

# include <math.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <errno.h>
# include "libft.h"
# include "mlx.h"

# include <stdio.h> // iuaeuiae

# include "settings.h"
# include "engine.h"
# include "controls.h"
# include "parser.h"
# include "messages.h"
# include "utils.h"

#endif
