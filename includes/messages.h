#ifndef MESSAGES_H
# define MESSAGES_H

# define ERROR "Error\n"
# define BLANK ""

# define W_MAP_ElEM "Unknow element on the map"
# define W_DOUBlE_PLAYER "You can't have two player on the map"

/*
** general informations 
** --------------------
*/

/*
** Missing identifier
*/

# define MNO "You didn't give any textures for the Nord wall"
# define MEA "You didn't give any textures for the East wall"
# define MSO "You didn't give any textures for the South wall"
# define MWE "You didn't give any textures for the Weast wall"
# define MS "You didn't give any textures for the sprite"
# define MR "You didn't give any resolution"
# define MC "You didn't give any color for the ceil"
# define MF "You didn't give any color for the floor"

/*
** Double identifier
*/

# define DID "You already define this identifier"

/*
** Wrong id
*/

# define WID "This identifier is unknow"

/*
** Textures
*/

# define DT "You provide more than one path for the texture"
# define EOT "A error oppening when trying to open the texture"

/*
** Resolution
*/

# define RXW "The x resolution have to be a positive number "
# define RYW "The y resolution have to be a positive number "
# define RF "Something get wrong on the resolution info"

/*
** Map error
*/

# define MDP "You already define a player"
# define MWC "This is not a map carac"
# define MMP "You need to give a player position"

# define MLX "Oups, Something get wrong with the minilibx"

# define NB_ERRORS 18

/*! \enum e_error
 *
 *  Detailed description
 */
enum e_error 
{  
	E_MNO,	
	E_MEA,
	E_MSO,
	E_MWE,
	E_MS,
	E_MR,
	E_MC,
	E_MF,
	E_DID,
	E_WID,
	E_MPD,
	E_MWC,
	E_MMP,
	E_DT,
	E_EOT,
	E_RXW,
	E_RYW,
	E_RF,
};

#endif
