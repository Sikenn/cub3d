#ifndef ENGINE_H
# define ENGINE_H

/*
** raycasting
*/
typedef struct		s_rct
{
	double			pos_y;
	double			pos_x;
	double			dir_x;
	double			dir_y;
	double			plane_x;
	double			plane_y;
	double			camera_x;
	double			ray_dir_y;
	double			ray_dir_x;
	double			side_dist_y;
	double			side_dist_x;
	double			delta_dist_y;
	double			delta_dist_x;
	double			perp_wall_dist;
	double			move_speed;
	double			rot_speed;
	int				hit;
	int				px;
	int				step_x;
	int				step_y;
	int				side;
	int				map_x;
	int				map_y;
	int				map_height;
	int				map_width;
	int				line_height;
	int				draw_start;
	int				draw_end;
	char			**map;
	t_mlx			win;
	t_mlx			tex;
	t_settings		*set;
	int				keys_state;
	int				update;
}					t_rct;

void				raycasting(t_rct *rct);

/*
** utils
*/
int make_color(int code, int rgb, int *color);

/*
** draw_pixels
*/
void draw_col(t_rct *rct, int color);
void push_col_to_image(t_rct *rct, int color);
void push_tex_to_image(t_rct *rct, t_mlx *tex, int tex_x);

void image(t_rct *rct);
void update_game(t_rct *rct);

void	init_window(t_mlx *mlx);
void	init_image(t_mlx *mlx);


#endif
