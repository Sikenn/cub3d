#ifndef CONTROLS_H
# define CONTROLS_H

/*
** keymap bepo arch
*/

# define ESC 65307
# define UP 233
# define RIGHT 105
# define DOWN 117
# define LEFT 97

# define ARROW_UP 65362
# define ARROW_RIGHT 65363
# define ARROW_DOWN 65364
# define ARROW_LEFT 65361

# define ACTION_FIRST 98
# define ACTION_SEC 112

# define NB_KEYS 7

int				keys_dispatcher(t_rct *rct);
int				key_release(int keycode, void *params);
int				key_press(int keycode, void *params);

typedef int		(*t_keys)(t_rct *);
int				esc(t_rct *rct);
int				up(t_rct *rct);
int				right(t_rct *rct);
int				down(t_rct *rct);
int				left(t_rct *rct);
int				rotate_right(t_rct *rct);
int				rotate_left(t_rct *rct);


#endif
