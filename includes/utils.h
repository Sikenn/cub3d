#ifndef UTILS_H
# define UTILS_H

void parse_error(t_settings *set, t_parsing *pars);
void set_error(t_parsing *pars, int err, int type_err);

/*! \enum e_type_error
 *
 *  Detailed description
 */
enum e_type_error 
{  
	FORMAT_ERROR,
	UNCOMPLET_ERROR,
	OTHER_ERROR
};

void errno_error(t_settings *set, int err);
void mlx_error(t_settings *set);
void	quit(t_settings *set);

#endif
