#ifndef PARSER_H
# define PARSER_H 

# define S_NO "NO"
# define S_EA "EA"
# define S_SO "SO"
# define S_WE "WE"
# define S_S "S"
# define S_R "R"
# define S_C "C"
# define S_F "F"


typedef struct	s_parsing
{
	int			state_info;
	int			state_file;
	int			cur_id;
	char		infos;
	int			err;
	int			type_err;
	int			i;

}				t_parsing;

/*! \enum e_state_file
 *
 *  Detailed description
 */
enum e_state_file 
{  
	INFOS,
	MAP
};

# define NB_ELEMS 8

# define S_NO "NO"
# define S_EA "EA"
# define S_SO "SO"
# define S_WE "WE"
# define S_S "S"
# define S_R "R"
# define S_C "C"
# define S_F "F"

enum e_state_elems
{
	E_NO,
	E_EA,
	E_SO,
	E_WE,
	E_S,
	E_R,
	E_C,
	E_F
};

# define NB_TYPES_STATE 4

enum e_state_type
{
	ID,
	TEX,
	COL,
	RES
};

typedef int		(*t_infos)(t_parsing *, t_settings *, char *);

void	parser(t_settings *set);
void	check_file(t_settings *set);
int		parse_map(t_parsing *pars, t_settings *set, char *line);
void	create_map_tab(t_settings *set);

#endif
