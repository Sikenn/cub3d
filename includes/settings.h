#ifndef SETTINGS_H
# define SETTINGS_H

# define SCREEN_WIDTH 640
# define SCREEN_HEIGHT 480
# define TEX_WIDTH 64
# define TEX_HEIGHT 64
# define NB_TEX 2
# define TITLE "Cub3d"
# define EXT ".cub"

# define MAP_HEIGTH 24
# define MAP_WIDTH 24

# define BLUE 0
# define GREEN 8
# define RED  16
# define R 0
# define G 1
# define B 2

# define NB_RGB 3
# define NB_MAP_ELEMS 8
# define EMPTY '0'
# define WALL '1'
# define SPR '2'
# define SPACE ' '
# define N 'N'
# define E 'E'
# define S 'S'
# define W 'W'

# define NORTH 0
# define EAST 1
# define SOUTH 2
# define WEST 3
# define SPRITE 4

typedef struct	s_mlx
{
		void	*mlx_ptr;
		void	*mlx_win;
		void	*img_ptr;
		int		bpp;
		int		size_line;
		int		endian;
		char	*img;
		int		width;
		int		height;

}				t_mlx;

typedef struct	s_settings
{
	char		*file_name;	
	int			fd;
	int			file_line;
	int			res_y;
	int			res_x;
	double		pos_y;
	double		pos_x;
	double		dir_y;
	double		dir_x;
	double		plane_y;
	double		plane_x;
	int			map_height;
	int			map_width;
	char		**map;
	t_mlx		win;
	t_mlx		tex[5]; 
	t_list		*list;
}				t_settings;


#endif
