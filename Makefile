#############
# VARIABLES #
#############

NAME 	= cub3d
CC		= clang
CFLAGS	+= -Wall 
CFLAGS  += -Wextra 
CFLAGS  += -Werror
MLXLINK += -L -lmlx -lXext -lX11 -lbsd
MLXLINK += -lm

############
# INCLUDES #
############

# internal
PATH_INCLUDES = includes
INCLUDES += -I$(PATH_INCLUDES)

# libft
PATH_LIBFT = libft
INCLUDES += -I$(PATH_LIBFT)/includes
LIBFT = $(PATH_LIBFT)/libft.a

# minilibX
PATH_MLX = minilibx-linux
INCLUDES += -I$(PATH_MLX)
MLX = $(PATH_MLX)/libmlx.a

###########
# Headers #
###########

HEADER += cub.h
HEADER += settings.h
HEADER += engine.h
HEADER += controls.h
HEADER += messages.h
HEADER += utils.h

vpath %.h $(PATH_INCLUDES)

#############
# SRCS PATH #
#############

PATH_SRCS = srcs
PATH_SRCS_ENGINE = $(PATH_SRCS)/engine
PATH_SRCS_CONTROLS = $(PATH_SRCS)/controls
PATH_SRCS_PARSER = $(PATH_SRCS)/parser
PATH_SRCS_UTILS = $(PATH_SRCS)/utils

########
# SRCS #
########

# core
SRCS += main.c

# utils

# engine
SRCS += raycasting.c
SRCS += draw_pixels.c
SRCS += utils.c 
SRCS += image.c 
SRCS += window.c 

# controls
SRCS += dispatcher.c
SRCS += key_controls.c

# parser
SRCS += parser.c
SRCS += check_file.c
SRCS += parse_map.c

# utils
SRCS += error_handler.c
SRCS += quit.c

##############
# ATRIBUTION #
##############

vpath %.c $(PATH_SRCS)
vpath %.c $(PATH_SRCS_ENGINE)
vpath %.c $(PATH_SRCS_CONTROLS)
vpath %.c $(PATH_SRCS_PARSER)
vpath %.c $(PATH_SRCS_UTILS)

###########
# OBJECTS #
###########

PATH_OBJS = objs/
OBJS = $(patsubst %.c, $(PATH_OBJS)%.o, $(SRCS))

#########
# RULES #
#########

all: $(PATH_OBJS) $(NAME)

$(NAME): $(OBJS) $(LIBFT) $(MLX)
	$(CC) $(MLXLINK) -o $@ $^ $(LIBFT) $(MLX)

$(OBJS): $(PATH_OBJS)%.o: %.c $(HEADER) Makefile
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

$(LIBFT):
	make -C $(PATH_LIBFT)

$(MLX):
	make -C $(PATH_MLX)

$(PATH_OBJS):
	mkdir $@

clean:
	make clean -C $(PATH_LIBFT)
	make clean -C $(PATH_MLX)
	$(RM) $(OBJS)
	$(RM) -R $(PATH_OBJS)

fclean: clean
	make fclean -C $(PATH_LIBFT)
	$(RM) $(NAME)

re: fclean all
.PHONY: all clean fclean re

