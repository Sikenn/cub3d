#include "cub.h"

int		esc(t_rct *rct)
{
	(void)rct;
	ft_printf("So sad you leave, see you soon!\n");
	quit(rct->set);
	return (TRUE);
}

int		up(t_rct *rct)
{
	if (rct->map[(int)(rct->pos_y + rct->dir_y * rct->move_speed)][(int)rct->pos_x] == EMPTY)
		rct->pos_y += rct->dir_y * rct->move_speed;
	if (rct->map[(int)rct->pos_y][(int)(rct->pos_x + rct->dir_x * rct->move_speed)] == EMPTY)
		rct->pos_x += rct->dir_x * rct->move_speed;
	return (TRUE);
}

int		right(t_rct *rct)
{
	if (rct->map[(int)(rct->pos_y)][(int)(rct->pos_x + rct->plane_x * rct->move_speed)] == EMPTY)
		rct->pos_y += rct->plane_y * rct->move_speed;
	if (rct->map[(int)(rct->pos_y + rct->plane_y * rct->move_speed)][(int)(rct->pos_x)] == EMPTY)
		rct->pos_x += rct->plane_x * rct->move_speed;
	return (TRUE);
}

int		left(t_rct *rct)
{

	if (rct->map[(int)(rct->pos_y)][(int)(rct->pos_x - rct->plane_x * rct->move_speed)] == EMPTY)
		rct->pos_y -= rct->plane_y * rct->move_speed;
	if (rct->map[(int)(rct->pos_y - rct->plane_y * rct->move_speed)][(int)(rct->pos_x)] == EMPTY)
		rct->pos_x -= rct->plane_x * rct->move_speed;
	return (TRUE);
}

int		down(t_rct *rct)
{
	if (rct->map[(int)(rct->pos_y - rct->dir_y * rct->move_speed)][(int)rct->pos_x] == EMPTY)
		rct->pos_y -= rct->dir_y * rct->move_speed;
	if (rct->map[(int)rct->pos_y][(int)(rct->pos_x - rct->dir_x * rct->move_speed)] == EMPTY)
		rct->pos_x -= rct->dir_x * rct->move_speed;
	return (TRUE);
}

int		rotate_right(t_rct *rct)
{
	double old_dir_x = rct->dir_x;
	rct->dir_x = rct->dir_x * cos(-rct->rot_speed) - rct->dir_y * sin(-rct->rot_speed);
	rct->dir_y = old_dir_x * sin(-rct->rot_speed) + rct->dir_y * cos(-rct->rot_speed);
	double old_plane_x = rct->plane_x;
	rct->plane_x = rct->plane_x * cos(-rct->rot_speed) - rct->plane_y * sin(-rct->rot_speed);
	rct->plane_y = old_plane_x * sin(-rct->rot_speed) + rct->plane_y * cos(-rct->rot_speed);
	return (TRUE);
}

int		rotate_left(t_rct *rct)
{
	double old_dir_x = rct->dir_x;
	rct->dir_x = rct->dir_x * cos(rct->rot_speed) - rct->dir_y * sin(rct->rot_speed);
	rct->dir_y = old_dir_x * sin(rct->rot_speed) + rct->dir_y * cos(rct->rot_speed);
	double old_plane_x = rct->plane_x;
	rct->plane_x = rct->plane_x * cos(rct->rot_speed) - rct->plane_y * sin(rct->rot_speed);
	rct->plane_y = old_plane_x * sin(rct->rot_speed) + rct->plane_y * cos(rct->rot_speed);
	return (TRUE);
}
