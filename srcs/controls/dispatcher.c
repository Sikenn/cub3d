#include "cub.h"

void print_params(t_rct *rct)
{
	printf("pos : [%lf][%lf]\n", rct->pos_y, rct->pos_x);
}

int	key_press(int keycode, void *params)
{
	int				i;
	t_rct			*rct;
	const int		keys[NB_KEYS] = {ESC, UP, RIGHT, DOWN, LEFT, ARROW_RIGHT, ARROW_LEFT};

	rct = (t_rct *)params;
	i = 0;
	while (i < NB_KEYS)
	{
		if (keycode == keys[i])
			rct->keys_state |= TRUE << i;
		i++;
	}
	return (SUCCESS);
}

int	key_release(int keycode, void *params)
{
	t_rct			*rct;
	int				i;
	const int		keys[NB_KEYS] = {ESC, UP, RIGHT, DOWN, LEFT, ARROW_RIGHT, ARROW_LEFT};

	rct = (t_rct *)params;
	i = 0;
	while (i < NB_KEYS)
	{
		if (keycode == keys[i])
			rct->keys_state &= ~(1UL << i);
		i++;
	}
	return (SUCCESS);
}

int		keys_dispatcher(t_rct *rct)
{
	const t_keys	controls[NB_KEYS] = {esc, up, right, down, left, rotate_right, rotate_left};
	int				i;

	i = 0;
	while (i < NB_KEYS)
	{
		if (rct->keys_state & TRUE << i)
		{
			controls[i](rct);
			return (TRUE);
		}
		i++;
	}
	return (FALSE);
}
