#include "cub.h"

// TODO : DEAL error
void	init_image(t_mlx *mlx)
{
	if (!(mlx->img_ptr = mlx_new_image(mlx->mlx_ptr, SCREEN_WIDTH, SCREEN_HEIGHT)
))
	{
		exit(0);
		//error(MLX_CRASH);
	}
	if (!(mlx->img = mlx_get_data_addr(mlx->img_ptr, &(mlx->bpp), &(mlx->size_line), &(mlx->endian))))
	{
		exit(0);
		//error(MLX_CRASH);
	}
}


void	init_window(t_mlx *mlx)
{
		if ((mlx->mlx_ptr = mlx_init()) == NULL)
		{
			exit(0);
			//error(MLX_CRASH);
		}
		if ((mlx->mlx_win = mlx_new_window(mlx->mlx_ptr, SCREEN_WIDTH, SCREEN_HEIGHT, TITLE)) == NULL)
		{
			//error(MLX_CRASH);
			exit(0);
		}
		//init_image(mlx);
}
