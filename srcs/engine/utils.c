#include "cub.h"

int make_color(int code, int rgb, int *color)
{
	if (code >= 0 && code <= 255)
	{
		*color |= code << rgb; 
		return (EXIT_SUCCESS);
	}
	ft_printf("Oups, something get wrong with the rgb code\n");
	return (EXIT_FAILURE);
}
