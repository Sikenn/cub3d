#include "cub.h"

/*
void draw_col(t_rct *rct, int color)
{
	while (rct->draw_start < rct->draw_end)
	{
		mlx_pixel_put(rct->mlx->mlx_ptr, rct->mlx->mlx_win, rct->px, rct->draw_start, color);
		rct->draw_start++;
	}
}
*/

void push_col_to_image(t_rct *rct, int color)
{
	t_mlx			*mlx;
	int				px_start;
	int				rgb;
	unsigned char	*c;
	int				y;

	c = (unsigned char *)&color;
	mlx = &rct->win;
	y = rct->draw_start;
	while (y < rct->draw_end)
	{
		px_start = (rct->px * 4) + (SCREEN_WIDTH * y * 4);
		rgb = 0;
		while (rgb < NB_RGB)
		{
			mlx->img[px_start + rgb] = c[rgb];
			rgb++;
		}
		y++;
	}
}

void push_tex_to_image(t_rct *rct, t_mlx *tex, int tex_x)
{
	int px_start;
	int rgb;
	int	tex_y;
	int y;


	y = rct->draw_start;
	while (y < rct->draw_end)
	{

		tex_y = (int)(y * 2 - SCREEN_HEIGHT + rct->line_height) 
			* ((tex->height / 2.0) / rct->line_height);

		int tex_start = (tex_x * 4) + (TEX_WIDTH * tex_y * 4);

		px_start = (rct->px * 4) + (SCREEN_WIDTH * y * 4);

		rgb = 0;
		while (rgb < NB_RGB)
		{
			rct->win.img[px_start + rgb] = tex->img[tex_start + rgb];
			rgb++;
		}
		y++;
	}
}
