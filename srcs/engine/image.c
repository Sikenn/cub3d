#include "cub.h"

void image(t_rct *rct)
{
	rct->win.img_ptr = mlx_new_image(rct->win.mlx_ptr, rct->set->res_x, rct->set->res_y);
	rct->win.img = mlx_get_data_addr(rct->win.img_ptr, &(rct->win.bpp), &(rct->win.size_line), &(rct->win.endian));
}

void update_game(t_rct *rct)
{
	if (rct->win.img_ptr)
		mlx_destroy_image(rct->win.mlx_ptr, rct->win.img_ptr);
	init_image(&rct->win);
	raycasting(rct);
	mlx_put_image_to_window(rct->win.mlx_ptr, rct->win.mlx_win, rct->win.img_ptr, 0, 0);
}
