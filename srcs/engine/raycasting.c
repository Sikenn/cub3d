#include "cub.h"

static void	calcul_side_dist(t_rct *rct)
{
	if (rct->ray_dir_x < 0.0)
	{
		rct->step_x = -1;
		rct->side_dist_x = (rct->pos_x - rct->map_x) * rct->delta_dist_x;
	}
	else
	{
		rct->step_x = 1;
		rct->side_dist_x = (rct->map_x + 1.0 - rct->pos_x) * rct->delta_dist_x;
	}
	if (rct->ray_dir_y < 0.0)
	{
		rct->step_y = -1;
		rct->side_dist_y = (rct->pos_y - rct->map_y) * rct->delta_dist_y;
	}
	else
	{
		rct->step_y = 1;
		rct->side_dist_y = (rct->map_y + 1.0 - rct->pos_y) * rct->delta_dist_y;
	}
}

static void	dda_algorithm(t_rct *rct)
{
	rct->hit = FALSE;
	while (rct->hit == FALSE)
	{
		if (rct->side_dist_x < rct->side_dist_y)
		{
			rct->side_dist_x += rct->delta_dist_x;
			rct->map_x += rct->step_x;
			rct->side = 0;
		//	rct->tex = rct->set->tex[WEST]; // mur rouge
			if (rct->step_x == 1)
				rct->tex = rct->set->tex[EAST]; // mur jaune
			else if (rct->step_x == -1)
				rct->tex = rct->set->tex[WEST]; // mur rouge
		}
		else
		{
			rct->side_dist_y += rct->delta_dist_y;
			rct->map_y += rct->step_y;
			rct->side = 1;
			if (rct->step_y == 1)
				rct->tex = rct->set->tex[NORTH]; // mur gris maron
			else if (rct->step_y == -1)
				rct->tex = rct->set->tex[SOUTH]; // mur gris
		}
		if (rct->map[rct->map_y][rct->map_x] == '1')
		{
			rct->hit = TRUE;
		}
	}
	if (rct->side == 0)
		rct->perp_wall_dist = fabs((rct->map_x - rct->pos_x + (1.0 - rct->step_x) / 2.0)
			/ rct->ray_dir_x);
	else
		rct->perp_wall_dist = fabs((rct->map_y - rct->pos_y + (1.0 - rct->step_y) / 2.0) 
			/ rct->ray_dir_y);
	rct->line_height = abs((int)(SCREEN_HEIGHT / rct->perp_wall_dist));
	rct->draw_start = ((-rct->line_height) / 2.0 + (SCREEN_HEIGHT / 2.0));
	if (rct->draw_start < 0)
		rct->draw_start = 0;
	rct->draw_end = (rct->line_height / 2.0 + (SCREEN_HEIGHT / 2.0));
	if (rct->draw_end >= SCREEN_HEIGHT)
			rct->draw_end = SCREEN_HEIGHT - 1;
}

void		raycasting(t_rct *rct)
{
	rct->px = 0;
	while (rct->px < SCREEN_WIDTH)	
	{
		rct->camera_x = 2.0 * rct->px / (double)SCREEN_WIDTH - 1;
		rct->ray_dir_x = rct->dir_x + rct->plane_x * rct->camera_x;
		rct->ray_dir_y = rct->dir_y + rct->plane_y * rct->camera_x;
		rct->map_x = (int)rct->pos_x;
		rct->map_y = (int)rct->pos_y;
		rct->delta_dist_x = fabs(1 / rct->ray_dir_x);
		rct->delta_dist_y = fabs(1 / rct->ray_dir_y);
		calcul_side_dist(rct);
		dda_algorithm(rct);
		double wall_x;

		if (rct->side == 0)
		{
			wall_x = rct->pos_y + rct->perp_wall_dist * rct->ray_dir_y;
		}
		else
		{
			wall_x = rct->pos_x + rct->perp_wall_dist * rct->ray_dir_x;
		}

		wall_x -= floor(wall_x);

		int tex_x = (int)(wall_x * (double)(rct->tex.width));

		if (rct->side == 0 && rct->ray_dir_x > 0.0)
			tex_x = rct->tex.width - tex_x - 1.0;
		else if (rct->side == 1 && rct->ray_dir_y < 0.0)
			tex_x = rct->tex.width - tex_x - 1.0;
		push_tex_to_image(rct, &rct->tex, tex_x);
		rct->px++;
	}
}
