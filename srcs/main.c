#include "cub.h"
#include <X11/Xlib.h>

void	init_rct(t_settings *set, t_rct *rct)
{
	rct->dir_x = set->dir_x;
	rct->dir_y = set->dir_y;
	rct->plane_x = set->plane_x;
	rct->plane_y = set->plane_y;
	rct->pos_y = set->pos_y;
	rct->pos_x = set->pos_x;
	rct->map_height = set->map_height;
	rct->map_width = set->map_width;
	rct->map = set->map;
	rct->move_speed = 0.08;
	rct->rot_speed = 0.02;
	rct->win = set->win;
	rct->set = set;
}

int	game_loop(void *params)
{
	t_rct *rct;
	static int	update = TRUE;

	rct = (t_rct *)params;
	if (update == TRUE)
		update_game(rct);
	update = (keys_dispatcher(rct) == TRUE) ? TRUE: FALSE;
	return (SUCCESS);
}

// TODO: free map && list

int	main(int ac, char **av)
{
 //   t_mlx		mlx;
	//t_mlx		tex;
	t_rct		rct;
	t_settings	set;

	if (ac == 2)
	{
		ft_bzero(&set, sizeof(t_settings));
		init_window(&set.win);
		set.file_name = av[1];
		parser(&set);
		//quit(&set);
		init_rct(&set, &rct);
		mlx_hook(rct.win.mlx_win, KeyPress, KeyPressMask, key_press, &rct);
		mlx_hook(rct.win.mlx_win, KeyRelease, KeyReleaseMask, key_release, &rct);
		mlx_loop_hook(rct.win.mlx_ptr, game_loop, &rct);
		mlx_loop(rct.win.mlx_ptr);
	}
	else
	{
		// TODO : error to deal
		// error(ARG_NB);
		exit(0);
	}
	return (EXIT_SUCCESS);
}
