#include "cub.h"

void	free_settings(t_settings *set)
{
	if (set->list)
		ft_lstclear(&set->list, free);
	if (set->map)
		free(set->map);
	set->map = NULL;
}

void	quit(t_settings *set)
{
	free_settings(set);	
	exit(0);
}
