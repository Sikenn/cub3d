#include "cub.h"

void set_error(t_parsing *pars, int err, int type_err)
{
	pars->err = err;
	pars->type_err = type_err;
}

void parse_error(t_settings *set, t_parsing *pars)
{
	const char *errors[NB_ERRORS] = {MNO, MEA, MSO, MWE, MS, MR, MC, MF, DID, WID, MDP, MWC, MMP, DT, EOT, RXW, RYW, RF};

	if (pars->type_err == FORMAT_ERROR)	
	{
		ft_printf("%sOn line[%d]: %s", 
				ERROR, set->file_line, errors[pars->err]);
	}
	else if (pars->type_err == UNCOMPLET_ERROR)
		ft_printf("%s%s", ERROR, errors[pars->err]);
	quit(set);
}

void mlx_error(t_settings *set)
{
	ft_printf("%s%s", ERROR, MLX);
	quit(set);
}

void errno_error(t_settings *set, int err)
{
	ft_printf("%s%s", ERROR, strerror(err));
	quit(set);
}
