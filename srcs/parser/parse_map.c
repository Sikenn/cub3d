#include "cub.h"

void	create_map_tab(t_settings *set)
{
	int	i;
	t_list *tmp;

	if (!(set->map = (char **)malloc(set->map_height * sizeof(char *))))
		errno_error(set, errno);
	i = 0;
	tmp = set->list;
	while (i < set->map_height && tmp) 
	{
		set->map[i] = (char *)tmp->content;
		tmp = tmp->next;
		i++;			
	}
}

void	set_player(t_settings *set, int x, char dir)
{
	set->pos_y = set->map_height;
	set->pos_x = x;
	// TODO: SHould be better
	if (dir == N)
	{
		set->dir_y = -1.0;
		set->dir_x = 0.0;
		set->plane_y = 0.66;
		set->plane_x = 0.0;
	}
	else if (dir == E)
	{
		set->dir_y = 0.0;
		set->dir_x = 1.0;
		set->plane_y = 0.0;
		set->plane_x = 0.66;
	}
	else if (dir == S)
	{
		set->dir_y = -1.0;
		set->dir_x = 0.0;
		set->plane_y = -0.66;
		set->plane_x = 0.0;
	}
	else if (dir == W)
	{
		set->dir_y = 0.0;
		set->dir_x = -1.0;
		set->plane_y = 0.66;
		set->plane_x = 0.0;
	}
}

int		check_elem(t_parsing *pars, t_settings *set, char elem, int x)
{
	const char	elems[NB_MAP_ELEMS] = {EMPTY, WALL, SPR, SPACE, N, E, S, W};
	int			i;

	i = 0;
	while (i < NB_MAP_ELEMS)
	{
		if (i < 4 && elem == elems[i])
			return (SUCCESS);
		else if (elem == elems[i])
		{
			if (set->pos_y && set->pos_x)
			{
				set_error(pars, E_MPD, FORMAT_ERROR);
				return (FAILURE);
			}
			set_player(set, x, elem);
			return (SUCCESS);
		}
		i++;
	}
	set_error(pars, E_MWC, FORMAT_ERROR);
	return (FAILURE);
}


int		check_line(t_parsing *pars, t_settings *set, char *line)
{
	int			i;

	i = 0;
	while (line[i]) 
	{
		if (check_elem(pars, set, line[i], i) == FAILURE)
			return (FAILURE);
		i++;	
	}
	set->map_width = (i > set->map_width) ? i : set->map_width;
	return (SUCCESS);
}

int		parse_map(t_parsing *pars, t_settings *set, char *line)
{
	t_list *new;
	char	*s;

	set->map_height++;
	if (check_line(pars, set, line) == FAILURE)
		return(FAILURE);
	else
	{
		if (!(s = ft_strdup(line)))
		{
			errno_error(set, errno);
			return (FAILURE);
		}
		if (!(new = ft_lstnew((char *)s)))
		{
			errno_error(set, errno);
			return (FAILURE);
		}
		ft_lstadd_back(&set->list, new);
		return(SUCCESS);
	}
}
