#include "cub.h"

void	print_bits(unsigned char octet)
{
	int				i;
	unsigned char	c;

	i = 128;
	while (i > 0)
	{
		if (octet < i)
		{
			c = '0';
			write(1, &c, 1);
			i /= 2;
		}
		else
		{
			c = '1';
			write(1, &c, 1);
			octet = octet - i;
			i /= 2;
		}
	}
}

int		miss_infos(char info)
{
	int		i;

	i = 0;
	//print_bits(info);
	//ft_printf("\n");
	while (i < NB_ELEMS) 
	{
		if (!(info & TRUE << i))					
		{
		//	ft_printf("%s %d: SUCCESS\n", __func__, i);
			return (i);
		}
		i++;		
	}
	//ft_printf("%s: FAILURE\n", __func__);
	return (FAILURE);
}

int		already_give_info(char infos, int info)
{
	if (infos & TRUE << info)
		return (TRUE);
	return (FALSE);
}

void	skip_space(t_parsing *pars, char *line)
{
	while (line[pars->i] == SPACE)
		pars->i++;
}

int		tex(t_parsing *pars, t_settings *set, char *line)
{
	(void)set;
	int		i;

	char rem;

	//ft_printf("i[%d] line [%s]\n", i, line);
	skip_space(pars, line);
	if (already_give_info(pars->infos, pars->cur_id))
	{
		set_error(pars, E_DT, FORMAT_ERROR);
		return (FAILURE);
	}
	i = pars->i;
	while (line[i] && line[i] != SPACE)
		i++;
	rem = line[i];
	line[i] = '\0';
	ft_printf("tex[%d]\n", pars->cur_id);
	if (!(set->tex[pars->cur_id].img_ptr = mlx_xpm_file_to_image(
				set->win.mlx_ptr,
				line + pars->i,
				&(set->tex[pars->cur_id].width),
				&(set->tex[pars->cur_id].height))))
	{
		set_error(pars, E_EOT, FORMAT_ERROR);
		return(FAILURE);
	}
	if (!(set->tex[pars->cur_id].img = mlx_get_data_addr(
					set->tex[pars->cur_id].img_ptr,
					&set->tex[pars->cur_id].bpp,
					&set->tex[pars->cur_id].size_line,
					&set->tex[pars->cur_id].endian)))
	{
		return(FAILURE);
	}
//	ft_printf("i[%d], line [%s]\n", i, line + i);
	line[i] = rem;
	pars->i += i;
	pars->infos |= TRUE << pars->cur_id;
	return (SUCCESS);	
}

void	skip_number(t_parsing *pars, char *line)
{
	while (ft_isdigit(line[pars->i])) 
		pars->i++;
}

int		col(t_parsing *pars, t_settings *set, char *line)
{
	//ft_printf("%s\n", __func__);
	(void)set;
		skip_space(pars, line);
	while (line[pars->i])
	{
		pars->i++;
	}
	pars->infos |= TRUE << pars->cur_id;
		//	ft_printf("%s: success\n", __func__);
	return (SUCCESS);	

}

int		res(t_parsing *pars, t_settings *set, char *line)
{
	//ft_printf("%s\n", __func__);
	(void)set;
	skip_space(pars, line);
	if (!ft_isdigit(line[pars->i]))
	{
		set_error(pars, E_RXW, FORMAT_ERROR);
		return (FAILURE);
	}
	set->res_x = ft_atoi(line + pars->i);
	skip_number(pars, line);
	skip_space(pars, line);
	if (!ft_isdigit(line[pars->i]))
	{
		set_error(pars, E_RYW, FORMAT_ERROR);
		return (FAILURE);
	}
	set->res_y = ft_atoi(line + pars->i);
	skip_number(pars, line);
	skip_space(pars, line);
	if (line[pars->i])
	{
		set_error(pars, E_RF, FORMAT_ERROR);
		return (FAILURE);
	}
	pars->infos |= TRUE << pars->cur_id;
			//ft_printf("%s: success\n", __func__);
	return (SUCCESS);	

}

int		id(t_parsing *pars, t_settings *set, char *line)
{
	const char *id[NB_ELEMS] = {S_NO, S_EA, S_SO, S_WE, S_S, 
		S_R, S_C, S_F};
	int			i;
	int			len;

	(void)set;
	i = 0;
	while (i < NB_ELEMS)
	{
		len = (i < 4) ? 2 : 1;	
			//ft_printf("len[%d]i[%d]   %s vs %s\n", len, i, line, id[i]);
		if (ft_strnequ(line, id[i], len))
		{
			if (already_give_info(pars->infos, i))
			{
				set_error(pars, E_DID, FORMAT_ERROR);
			//	ft_printf("%s: failure 1\n", __func__);
				return (FAILURE);
			}
			pars->i += len;
			pars->cur_id = i;
			if (i < 5)
				pars->state_info = TEX;
			else if (i == 5)
				pars->state_info = RES;
			else
				pars->state_info = COL;
		//	ft_printf("%s: success\n", __func__);
			return (SUCCESS);
		}
		i++;
	}
	set_error(pars, E_WID, FORMAT_ERROR);
	//ft_printf("%s: failure 2\n", __func__);
	return (FAILURE);
}

int		parse_info(t_parsing *pars, t_settings *set, char *line)
{
	const t_infos	info[NB_TYPES_STATE] = {id, tex, col, res};

	pars->i = 0;
	pars->state_info = ID;
	while (line[pars->i])
	{
		if ((info[pars->state_info](pars, set, line)) == FAILURE)
		{
		//	ft_printf("%s: failure\n", __func__);
			return (FAILURE);
		}
	}
	//ft_printf("%s: success\n", __func__);
	return (SUCCESS);
}

void	parser(t_settings *set)
{
	t_parsing	pars;
	int			ret_line;
	int			ret_info;
	char		*line;

	check_file(set);
	ft_bzero(&pars, sizeof(t_parsing));
	// TODO : deal error
	ret_line = TRUE;
	pars.state_file = INFOS;
	while (ret_line) 
	{
		if (miss_infos(pars.infos) == FAILURE)
			pars.state_file = MAP;
		ret_line = get_next_line(set->fd, &line);	
		set->file_line++;
		// TODO: handle error here
		ret_info = (pars.state_file == INFOS) ? 
			parse_info(&pars, set, line) : parse_map(&pars, set, line);
		free(line);
		if (ret_info == FAILURE)
			break ;
	}
	if (ret_info == FAILURE)
		parse_error(set, &pars);
	if (miss_infos(pars.infos) != FAILURE)
	{
		set_error(&pars, miss_infos(pars.infos), UNCOMPLET_ERROR);
		parse_error(set, &pars);
	}
	if (set->pos_x == 0 && set->pos_y == 0)
	{
		set_error(&pars, E_MMP, UNCOMPLET_ERROR);
		parse_error(set, &pars);
	}
	create_map_tab(set);
	close(set->fd);
}
